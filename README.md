# ScriptRunner delegated repository creation example  #

This repository contains the example code for adding a button/modal for repository creation using ScriptRunner fragments.

The example code here should be seen as a starting point, one that can be expanded on with additional functionality / security. This is just a proof of concept.
### What is each file for? ###

* createRepository.js (JavaScript to be installed as a web resource, this handles contacting the REST endpoint for repository creation)
* rest-endpoint.groovy (ScriptRunner REST endpoint which handles creating repositories, and sending the modal HTML to be shown after clicking on the web item button.)
* web-item-conditon.groovy (Condition code that can be used within web item to ensure repository creation button is only shown to users in a specific group)

# Configurations to setup:

![REST endpoint](rest-endpoint.png)

![Custom web resource](web-resource.png)

![Custom web item](custom-web-item.png)