AJS.toInit(function() {
  function getProjectKey() {
    var bitbucketState = require("bitbucket/util/state");
 
    return bitbucketState.getProject().key;
  }
 
  function createRepository() {
    var createRepositoryDialog = AJS.$("#sr-create-repo-dialog");
 
    var projectKey = getProjectKey();
 
    var repositoryName = AJS.$(createRepositoryDialog)
      .find("#name")
      .val();
 
    AJS.$.ajax({
      url:
        AJS.contextPath() + "/rest/scriptrunner/latest/custom/createRepository",
      headers: {
        "X-Atlassian-Token": "no-check"
      },
      type: "POST",
      data: JSON.stringify({
        projectKey: projectKey,
        repositoryName: repositoryName
      }),
      contentType: "application/json",
      dataType: "json",
      success: function(data, textStatus, request) {
        window.location.href = request.getResponseHeader("Location");
      },
      error: function(error) {
        console.log(error);
      }
    });
  }
 
  function setupCreateRepositoryDialog() {
    AJS.dialog2.on("show", function(event) {
      var targetId = event.target.id;
 
      if (targetId == "sr-create-repo-dialog") {
        var createRepositoryDialog = AJS.dialog2(event.target);
 
        AJS.$(event.target)
          .find("#sr-create-repo-create-button")
          .click(createRepository);
 
        AJS.$(event.target)
          .find("form")
          .submit(function(e) {
            e.preventDefault();
            createRepository();
          });
 
        AJS.$(event.target)
          .find("#sr-create-repo-close-button")
          .click(function(e) {
            e.preventDefault();
            createRepositoryDialog.hide();
            createRepositoryDialog.remove();
          });
      }
    });
  }
 
  setTimeout(setupCreateRepositoryDialog, 0);
});
