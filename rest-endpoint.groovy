
import com.atlassian.bitbucket.repository.RepositoryService
import com.atlassian.bitbucket.user.SecurityService
import com.atlassian.bitbucket.nav.NavBuilder
import com.atlassian.bitbucket.project.ProjectService
import com.atlassian.bitbucket.repository.RepositoryCreateRequest
import com.atlassian.bitbucket.permission.Permission
import com.atlassian.sal.api.component.ComponentLocator
import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import org.codehaus.jackson.map.ObjectMapper
import groovy.json.JsonBuilder
import groovy.transform.BaseScript
 
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response
import javax.ws.rs.core.MediaType
 
@BaseScript CustomEndpointDelegate delegate
 
createRepository(httpMethod: "GET", groups: ["repo-creators"]) { MultivaluedMap queryParams, String body ->
        def dialog =
        """<section role="dialog" id="sr-create-repo-dialog" class="aui-layer aui-dialog2 aui-dialog2-medium" aria-hidden="true" data-aui-remove-on-hide="true">
            <header class="aui-dialog2-header">
                <h2 class="aui-dialog2-header-main">Create a repository</h2>
                <a class="aui-dialog2-header-close">
                    <span class="aui-icon aui-icon-small aui-iconfont-close-dialog">Close</span>
                </a>
            </header>
            <div class="aui-dialog2-content">
                <form class="aui prevent-double-submit  stash-repository-create-form" action="" method="post" accept-charset="UTF-8">
                   <div class="field-group">
                      <label for="name">Name<span class="aui-icon icon-required"><span>(required)</span></span></label><input class="text" type="text" id="name" name="name" autofocus="" autocomplete="off" maxlength="128">
                      <div class="description">
                         The repository's name will be used to create its URL
                         <div class="clone-url-generated"><span></span></div>
                      </div>
                   </div>
                </form>
            </div>
            
            <footer class="aui-dialog2-footer">
                <!-- Actions to render on the right of the footer -->
                <div class="aui-dialog2-footer-actions">
                    <button class="aui-button aui-button-primary" id="sr-create-repo-create-button">Create repository</button>
                    <button id="sr-create-repo-close-button" class="aui-button aui-button-link">Cancel</button>
                </div>
            </footer>
        </section>
        """
 
    Response.ok().type(MediaType.TEXT_HTML).entity(dialog.toString()).build()
}
 
class CreateRepositoryRequest {
    String projectKey
    String repositoryName
}
 
createRepository(httpMethod: "POST", groups: ["repo-creators"]) { MultivaluedMap queryParams, String body ->
    def objectMapper = new ObjectMapper()
    def createRequest = objectMapper.readValue(body, CreateRepositoryRequest)
     
    def projectService = ComponentLocator.getComponent(ProjectService)
    def repositoryService = ComponentLocator.getComponent(RepositoryService)
     
     
    def project = projectService.getByKey(createRequest.projectKey)
     
    def repositoryCreationRequest = new RepositoryCreateRequest.Builder()
        .project(project)
        .name(createRequest.repositoryName)
        .scmId("git")
        .build()
     
    def securityService = ComponentLocator.getComponent(SecurityService)
     
    def createdRepo = securityService.withPermission(Permission.PROJECT_ADMIN, "Delegated repository creation").call {
        repositoryService.create(repositoryCreationRequest)
    }
      
    def navBuilder = ComponentLocator.getComponent(NavBuilder)
    def repoUrl = navBuilder.repo(createdRepo).buildAbsolute()
     
    Response.created(new URI(repoUrl)).build()
}
