import com.atlassian.sal.api.user.UserManager
import com.atlassian.sal.api.component.ComponentLocator

def userManager = ComponentLocator.getComponent(UserManager)

userManager.isUserInGroup(userManager.remoteUserKey, "repo-creators")
